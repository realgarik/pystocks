#############################################################
# This file is used to provide API for market data requests
#############################################################
import requests

# Unique id retrieved from Market Data for the registered account
API_TOKEN = "&api_token=PWZLtkgEdMD8jDYlyukRQYofNeDH5HNVHxC9LIT6fGjJBRWUoC8oWCEpHiB6"

# link: https://www.worldtradingdata.com/documentation#stock-and-index-intraday
#
# Returns intraday data for a single stock or index in the range
# with the specified interval
#
# symbol[in]   - single stock or index
# interval[in] - Number of minutes between the data.
#                should be one of the following values: 1, 2, 5, 15, 60
# range[in]    - The number of days data is returned for.
#                supported days are in range from 1-30
#
def getIntraday(symbol, interval, range):

    print("Prepare Intraday request for ", symbol)

    # Symbol of the company which stock data going to be retrieved
    SYMBOL = "symbol=" + symbol
    # Number of minutes between the data, 1-60
    INTERVAL = "&interval=" + interval
    # The number of days data is returned for, 1-30
    RANGE = "&range=" + range

    # To retrieve starting from the fixed time with specified interval
    URL = "https://intraday.worldtradingdata.com/api/v1/intraday"
    PARAMS = SYMBOL + RANGE + INTERVAL + API_TOKEN

    return requests.get(URL, PARAMS)



# link: https://www.worldtradingdata.com/documentation#real-time-market-data
#
# Returns the nearest trading data for stocks and indexes worldwide
#
# symbol[in]   - single stock or index
#
def getStockRealTime(symbol):
    print("Prepare Stock Real Time request for ", symbol)

    # FIXME: Doesn't tested

    SYMBOL = "symbol=" + symbol
    URL = "https://api.worldtradingdata.com/api/v1/stock"
    PARAMS = SYMBOL + API_TOKEN

    return requests.get(URL, PARAMS)


# link: https://www.worldtradingdata.com/documentation#mutual-fund-real-time
#
# The endpoint allows up to 500 mutual funds to be returned
# with each request in exceptional timing.
#
# symbol[in]   - single stock or index
#
def getMutualFundRealTime(symbol):
    print("Prepare Mutual Fund Real Time request for ", symbol)

    # FIXME: Doesn't tested

    SYMBOL = "symbol=" + symbol
    URL = "https://api.worldtradingdata.com/api/v1/mutualfund"
    PARAMS = SYMBOL + API_TOKEN

    return requests.get(URL, PARAMS)

# link: https://www.worldtradingdata.com/documentation#historical-market-data
#
# Our historical data endpoint returns the end of day history for every day the stock,
# index or mutual fund has been traded. The endpoint allows you to enter to and from dates
# to refine the data that will be returned and you can also sort the data by newest or oldest.
#
#
#
def getHistoricalMarketData(symbol, sort):
    print("Prepare Real Time request for ", symbol)

    # FIXME: Doesn't tested

    SYMBOL = "symbol=" + symbol
    URL = "https://api.worldtradingdata.com/api/v1/history"
    PARAMS = SYMBOL + API_TOKEN

    return requests.get(URL, PARAMS)
