# importing the required module
import matplotlib.pyplot as plt

def printGraphics(mapToPrint):
    plt.plot(list(mapToPrint.keys()), list(mapToPrint.values()))

    # naming the x axis
    plt.xlabel('stock value')
    # naming the y axis
    plt.ylabel('time')

    # giving a title to my graph
    plt.title('Value / Time Graphics')

    # function to show the plot
    plt.show()
