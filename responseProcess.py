########################################################
# This file is used to process requested market data
########################################################
from collections import defaultdict
from operator import itemgetter
import graphics

# Sort starting from the lowest number and returns map of times
# for specified values
def timeForLowestValues(intraday, RANGE):
    low_dict_list = [defaultdict() for x in range(int(RANGE))]

    print("---------------------------------------")
    count = 0
    eng_static = 0

    for item in intraday:
        if eng_static == 0:
            prev_date = item[:10]
            eng_static += 1
            #print('---- Prev Date initiated: ', prev_date, 'cur_item: ', item[:10])

        if item[:10] == prev_date:
            low_dict_list[count].update({item: intraday[item]['low']})
        else:
            count += 1
            prev_date = item[:10]
            print(prev_date)
            low_dict_list[count].update({item: intraday[item]['low']})

    for iter in low_dict_list:
        #print(iter)
        # Sort based on value
        od_low = sorted(iter.items(), key=itemgetter(1))
        print(od_low)
        #graphics.printGraphics(od_low)


    print("---------------------------------------")